Walkthrough from:
    https://engineering.riotgames.com/news/thinking-inside-container

Hybird from the above walkthrough, retaining official images in order to receive updates but with a few changes and tweaks. Uses the BlueOcean image which contains the plugin built in, since that's what we're running here

Requirements:
- Docker installed locally
    - `brew install docker docker-machine docker-compose`
- Certificate created and installed to ~/jenkins-data/certs:
    - `openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ~/jenkins-data/certs/cert.key -out  ~/jenkins-data/certs/cert.crt`

Should create a usable Jenkins environment with simply:
- make build && make run
- and then browse to: https://127.0.0.1